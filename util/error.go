package util

import "github.com/go-playground/log/v8"

// 处理err【打印】
func DealError(err error, funcname ...string) {
	log.Error("yua ", funcname, " err=%+v", err.Error())
}
