package util

import (
	"fmt"
	"testing"
)

func TestCheckPhone(t *testing.T) {
	phone := "13511122322"

	if CheckPhone(phone) {
		fmt.Println("OK")
	} else {
		fmt.Println("NO")
	}
}

func TestCheckEmail(t *testing.T) {
	ipt := "yauua@hqh.cn"

	if CheckEmail(ipt) {
		fmt.Println("OK")
	} else {
		fmt.Println("NO")
	}
}
