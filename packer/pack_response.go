package packer

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/haoranz527/gocommon/constant"
	"net/http"
)

func MakeOkResponse(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"data":    data,
		"message": constant.OK_RESPONSE_MESSAGE,
		"code":    constant.OK_RESPONSE_CODE,
	})
}

func MakeErrorResponse(c *gin.Context, message string) {
	c.JSON(http.StatusOK, gin.H{
		"message": func() string {
			if message == "" {
				return constant.ERROR_RESPONSE_MESSAGE
			}
			return message
		}(),
		"code": constant.ERROR_RESPONSE_CODE,
	})
}

func MakeQueryErrorResponse(c *gin.Context, message string) {
	c.JSON(http.StatusOK, gin.H{
		"data": func() string {
			if message != "" {
				return message
			}
			return constant.QUERY_ERROR_RESPONSE_MSG
		},
		"message": constant.QUERY_ERROR_RESPONSE_CODE,
		"code":    constant.QUERY_ERROR_RESPONSE_CODE,
	})
}
