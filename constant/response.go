package constant

// message
const (
	OK_RESPONSE_MESSAGE      = "sucess"
	ERROR_RESPONSE_MESSAGE   = "FAIL"
	QUERY_ERROR_RESPONSE_MSG = "some query error"
)

// code
const (
	OK_RESPONSE_CODE          = 200
	ERROR_RESPONSE_CODE       = 1002
	QUERY_ERROR_RESPONSE_CODE = 1003
)
