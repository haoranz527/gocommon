package model

import "fmt"

// 数据库中的存储模式
type TUser struct {
	BizId    int64  `json:"biz_id"`
	UserId   int64  `json:"user_id"`
	UserName string `json:"user_name"`
	Phone    string `json:"phone"`
	Email    string `json:"email"`
	Pwd      string `json:"pwd"`
	Image    string `json:"image"`
	Status   int    `json:"status"`
}

func NewTUser(bizId int64, userId int64, userName string, phone string, email string, pwd string, status int) *TUser {
	return &TUser{BizId: bizId, UserId: userId, UserName: userName, Phone: phone, Email: email, Pwd: pwd, Status: status}
}

func m() {
	fmt.Println("Hello")
}
